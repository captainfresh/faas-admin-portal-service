FROM maven:3.6.0-jdk-11-slim AS build
COPY  * /home/app/
COPY src /home/app/src
COPY .mvn /home/app/.mvn
WORKDIR /home/app/
RUN mvn clean package

FROM openjdk:11-jre-slim
COPY --from=build /home/app/target/*.jar /usr/local/lib/app.jar
EXPOSE 9012
ENTRYPOINT ["java","-jar","/usr/local/lib/app.jar"]
