package com.admin.app.repository;

import com.admin.app.domain.*;
import feign.Headers;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

@Headers("Content-Type: application/json")
@FeignClient(name = "fms", url= "${abbreviation.fms}")
public interface FmsRepository {
    @RequestMapping(value = "/api/facilities", method = RequestMethod.GET)
    List<Facility> facilities();

    @RequestMapping(value = "/api/workstations", method = RequestMethod.GET)
    List<Workstation> workstations(@SpringQueryMap Map<String, String> workstationQueryMap, Pageable pageable);

    @RequestMapping(value = "/api/areas", method = RequestMethod.GET)
    List<AreaWithWorkstation> Areas();

    @RequestMapping(value = "/api/areas/all",method = RequestMethod.GET)
    ResponseEntity<List<Area>> areas(@SpringQueryMap Map<String, String> areaQueryMap, Pageable pageable);

    @RequestMapping(value = "/api/update-area-bulk",method = RequestMethod.PUT)
    String updateArea(@RequestBody  List<Area> requestBody);

    @RequestMapping(value = "/api/update-workstation-bulk" , method = RequestMethod.PUT)
    String updateWorkstation(@RequestBody Set<Workstation> workstations);

    @RequestMapping(value = "/api/update-storage-bulk" , method = RequestMethod.PUT)
    String updateStorage(@RequestBody Set<Storage> storages);

    @RequestMapping(value = "/api/facilities" ,method = RequestMethod.POST)
    Facility createFacility(@RequestBody Facility facility);

    @RequestMapping(value = "/api/areas" ,method = RequestMethod.POST)
    Area createArea(@RequestBody Area area);

    @RequestMapping(value = "/api/workstations" ,method = RequestMethod.POST)
    Workstation createWorkstation(@RequestBody Workstation workstation);

    @RequestMapping(value = "/api/facilities/{id}" ,method = RequestMethod.GET)
    Facility getFacilityById(@PathVariable("id") UUID id);






}
