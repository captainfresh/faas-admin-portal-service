package com.admin.app.repository;

import com.admin.app.domain.*;
import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.UUID;

@Headers("Content-Type: application/json")
@FeignClient(name = "fms", url = "${abbreviation.tasker}")
public interface TaskerRepository {

    @RequestMapping(value = "/api/activity-entity-mappings",method = RequestMethod.GET)
    List<ActivityEntityMapping> activities();

    @RequestMapping(value = "/api/activity-entity-mapping-bulk" , method = RequestMethod.POST)
    String addActivity(@RequestBody List<ActivityEntityMapping> activityEntityMappings);

    @RequestMapping(value = "/api/activity-entity-mapping-bulk" , method = RequestMethod.DELETE)
    String deleteActivity(@RequestBody List<ActivityEntityMapping> activityEntityMappings);


}
