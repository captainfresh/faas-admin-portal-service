package com.admin.app.web.rest;

import com.admin.app.domain.*;
import com.admin.app.domain.Enumeration.AreaType;
import com.admin.app.repository.FmsRepository;
import com.admin.app.repository.TaskerRepository;
import io.vavr.concurrent.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api")
public class WorkstationResource {

    public TaskerRepository taskerRepository;
    public FmsRepository fmsRepository;

    public WorkstationResource(TaskerRepository taskerRepository, FmsRepository fmsRepository){
        this.taskerRepository = taskerRepository;
        this.fmsRepository = fmsRepository;
    }

    @GetMapping("/workstation-with-task")
    public List<Workstation> getWorkstation(@RequestParam Map<String, String> workstationParams, Pageable pageable){
        //System.out.println(fmsRepository.facilities());
        List<Workstation> workstations =  fmsRepository.workstations(workstationParams, pageable);
        List<ActivityEntityMapping> activities = taskerRepository.activities();

        for(Workstation workstation : workstations){

            for(ActivityEntityMapping activityEntity : activities){
                //System.out.println(activityEntity.getEntityType() +" "+ activityEntity.getEntityId() + " " + workstation.getId());
                if(activityEntity.getEntityType().equals("workstation") && activityEntity.getEntityId().equals(workstation.getId()) ){

                    workstation.addActivity(activityEntity.getActivity());
                }

            }

        }
        return workstations;
    }

    @GetMapping("/area-with-workstation-storage")
    public ResponseEntity<List<Area>> getAreas( @RequestParam Map<String, String> areaParams, Pageable pageable) {
        ResponseEntity<List<Area>> areasResponse = fmsRepository.areas(areaParams, pageable);
        List<ActivityEntityMapping> activities = taskerRepository.activities();
        for(Area area : Objects.requireNonNull(areasResponse.getBody())){
            if(area.getType().equals(AreaType.STORAGE_AREA)){
                for(ActivityEntityMapping activity : activities){
                    if(activity.getEntityType().equals("area") && activity.getEntityId().equals(area.getId())){

                        area.addActivity(activity.getActivity());
                    }
                }
            }
        }
        return areasResponse;
    }


    @PutMapping("/set-workstation-storage-with-area")
    public ResponseEntity<Area> setWsStorageForArea(@RequestBody Area area){

        Set<Workstation> workstations = area.getWorkstations();
        Set<Storage> storages = area.getStorages();
        Set<Activity> activities = area.getActivities();
         if(area.getId() == null){
            area = fmsRepository.createArea(area);
        }

        if(area.getType().equals(AreaType.WORKSTATION_AREA)){

            if(workstations != null){
                for(Workstation workstation : workstations){
                    workstation.setArea(area);
                }
                fmsRepository.updateWorkstation(workstations);
            }

        }else if(area.getType().equals(AreaType.STORAGE_AREA)){

            if(storages != null){
                for(Storage storage : storages){
                    storage.setArea(area);
                }
            }
            fmsRepository.updateStorage(storages);
            List<ActivityEntityMapping> activityEntityMappings = new ArrayList<>();

            List<ActivityEntityMapping> toBeAdded = new ArrayList<>();
            List<ActivityEntityMapping> toBeDeleted = new ArrayList<>();

            for(Activity activity: activities){
                ActivityEntityMapping activityEntityMapping = new ActivityEntityMapping();
                activityEntityMapping.setEntityId(area.getId());
                activityEntityMapping.setEntityType("area");
                activityEntityMapping.setActivity(activity);
                activityEntityMappings.add(activityEntityMapping);

                if(activity.getAction().equals("add")){

                    toBeAdded.add(activityEntityMapping);
                }else if(activity.getAction().equals("remove")){

                    toBeDeleted.add(activityEntityMapping);
                }

            }
            taskerRepository.addActivity(toBeAdded);
            taskerRepository.deleteActivity(toBeDeleted);
        }


        return ResponseEntity.ok().body(area);

    }

    @PutMapping("/set-workstation-with-task")
    public ResponseEntity<Workstation> setActivityForWorkstation(@RequestBody Workstation workstation){
        List<ActivityEntityMapping> toBeAdded = new ArrayList<>();
        List<ActivityEntityMapping> toBeDeleted = new ArrayList<>();
        List<Activity> activities = workstation.getActivities();

        if(workstation.getId() == null){
            workstation = fmsRepository.createWorkstation(workstation);
        }

        if(workstation.getActivities() != null){
            for(Activity activity : activities){
                ActivityEntityMapping activityEntityMapping = new ActivityEntityMapping();
                activityEntityMapping.setEntityType("workstation");
                activityEntityMapping.setEntityId(workstation.getId());
                activityEntityMapping.setActivity(activity);

                if(activity.getAction().equals("add")){

                    toBeAdded.add(activityEntityMapping);
                }else if(activity.getAction().equals("remove")){

                    toBeDeleted.add(activityEntityMapping);
                }
            }
            taskerRepository.addActivity(toBeAdded);
            taskerRepository.deleteActivity(toBeDeleted);
        }
        return ResponseEntity.ok().body(workstation);
    }
}
