/**
 * View Models used by Spring MVC REST controllers.
 */
package com.admin.app.web.rest.vm;
