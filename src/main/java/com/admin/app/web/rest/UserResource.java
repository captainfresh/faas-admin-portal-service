package com.admin.app.web.rest;

import com.admin.app.domain.request.ManageUserRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class UserResource {



    @PostMapping("/manage-user")
    public ResponseEntity<Void> createUser(@RequestBody ManageUserRequest userRequest) {

        /**
         *  Trigger manage user workflow
         */

        return ResponseEntity.ok(null);
    }

}
