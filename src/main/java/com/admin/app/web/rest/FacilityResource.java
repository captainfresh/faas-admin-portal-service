package com.admin.app.web.rest;

import com.admin.app.domain.Area;
import com.admin.app.domain.Facility;
import com.admin.app.repository.FmsRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api")
public class FacilityResource {

    private FmsRepository fmsRepository;

    public FacilityResource(FmsRepository fmsRepository){
        this.fmsRepository = fmsRepository;
    }

    @GetMapping("/status")
    public String status(){
        return "success";
    }


    @PostMapping(value = "/set-area-with-facility")
    public ResponseEntity<Facility> setAreaInFacility( @RequestBody Facility facility){
        List<Area> areas = facility.getAreas();
        if(facility.getId()== null){
            facility = fmsRepository.createFacility(facility);
        }
        if(areas != null) {
            for (Area area : areas) {
                if (area.getAction() != null && area.getAction().equals("add")) {
                    area.setFacility(facility);


                } else if (area.getAction() != null && area.getAction().equals("remove")) {
                    area.setFacility(null);

                }
            }
            fmsRepository.updateArea(areas);
        }
        return new ResponseEntity<>(facility , HttpStatus.OK);
    }
}
