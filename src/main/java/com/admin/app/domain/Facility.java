package com.admin.app.domain;

import com.admin.app.domain.Enumeration.FacilityType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;


import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Setter
@Getter
public class Facility implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID id;

    private String type;

    private String name;

    private UUID address;

    private String metadata;


    private UUID createdBy;

    private UUID updatedBy;

    private UUID deletedBy;

    @JsonIgnoreProperties(value = { "workstations", "storages", "facility" }, allowSetters = true)
    private List<Area> areas;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public UUID getId() {
        return this.id;
    }

    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public Facility id(UUID id) {
        this.setId(id);
        this.areas = new ArrayList<>();
        return this;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getType() {
        return this.type;
    }

    public Facility type(String type) {
        this.setType(type);
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public Facility name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getAddress() {
        return this.address;
    }

    public Facility address(UUID address) {
        this.setAddress(address);
        return this;
    }

    public void setAddress(UUID address) {
        this.address = address;
    }

    public String getMetadata() {
        return this.metadata;
    }

    public Facility metadata(String metadata) {
        this.setMetadata(metadata);
        return this;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public UUID getCreatedBy() {
        return this.createdBy;
    }

    public Facility createdBy(UUID createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(UUID createdBy) {
        this.createdBy = createdBy;
    }

    public UUID getUpdatedBy() {
        return this.updatedBy;
    }

    public Facility updatedBy(UUID updatedBy) {
        this.setUpdatedBy(updatedBy);
        return this;
    }

    public void setUpdatedBy(UUID updatedBy) {
        this.updatedBy = updatedBy;
    }

    public UUID getDeletedBy() {
        return this.deletedBy;
    }

    public Facility deletedBy(UUID deletedBy) {
        this.setDeletedBy(deletedBy);
        return this;
    }

    public void setDeletedBy(UUID deletedBy) {
        this.deletedBy = deletedBy;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and
    // setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Facility)) {
            return false;
        }
        return id != null && id.equals(((Facility) o).id);
    }

    @Override
    public int hashCode() {
        // see
        // https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }
    // prettier-ignore
    // @Override
    // public String toString() {
    // return "Facility{" + "id=" + getId() + ", type='" + getType() + "'" + ",
    // name='" + getName() + "'"
    // + ", address='" + getAddress() + "'" + ", metadata='" + getMetadata() + "'" +
    // ", createdAt='"
    // + getCreatedAt() + "'" + ", updatedAt='" + getUpdatedAt() + "'" + ",
    // deletedAt='" + getDeletedAt() + "'"
    // + ", createdBy='" + getCreatedBy() + "'" + ", updatedBy='" + getUpdatedBy() +
    // "'" + ", deletedBy='"
    // + getDeletedBy() + "'" + "}";
    // }
}
