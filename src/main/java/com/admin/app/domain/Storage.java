package com.admin.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.UUID;

public class Storage {

    private static final long serialVersionUID = 1L;

    private UUID id;

    private UUID aisle;

    private UUID rack;

    private UUID bin;

    private UUID column;

    private String metadata;
    private  Area area;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Storage id(UUID id) {
        this.id = id;
        return this;
    }

    public UUID getAisle() {
        return this.aisle;
    }

    public Storage aisle(UUID aisle) {
        this.aisle = aisle;
        return this;
    }

    public void setAisle(UUID aisle) {
        this.aisle = aisle;
    }

    public UUID getRack() {
        return this.rack;
    }

    public Storage rack(UUID rack) {
        this.rack = rack;
        return this;
    }

    public void setRack(UUID rack) {
        this.rack = rack;
    }

    public UUID getBin() {
        return this.bin;
    }

    public Storage bin(UUID bin) {
        this.bin = bin;
        return this;
    }

    public void setBin(UUID bin) {
        this.bin = bin;
    }

    public UUID getColumn() {
        return this.column;
    }

    public Storage column(UUID column) {
        this.column = column;
        return this;
    }

    public void setColumn(UUID column) {
        this.column = column;
    }

    public String getMetadata() {
        return this.metadata;
    }

    public Storage metadata(String metadata) {
        this.metadata = metadata;
        return this;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

// jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Storage)) {
            return false;
        }
        return id != null && id.equals(((Storage) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Storage{" +
            "id=" + getId() +
            ", aisle='" + getAisle() + "'" +
            ", rack='" + getRack() + "'" +
            ", bin='" + getBin() + "'" +
            ", column='" + getColumn() + "'" +
            ", metadata='" + getMetadata() + "'" +
            "}";
    }
}
