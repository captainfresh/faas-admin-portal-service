package com.admin.app.domain;

import com.admin.app.domain.Enumeration.AreaType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class Area implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID id;

    private String name;

    private AreaType type;

    private Set<Workstation> workstations = new HashSet<>();

    private Set<Storage> storages = new HashSet<>();

    private Set<Activity> activities = new HashSet<>();

    private Facility facility;

    private String action;

    public void setActivities(Set<Activity> activities) {
        this.activities = activities;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Area id(UUID id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public Area name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AreaType getType() {
        return this.type;
    }

    public Area type(AreaType type) {
        this.type = type;
        return this;
    }

    public void setType(AreaType type) {
        this.type = type;
    }

    public Set<Workstation> getWorkstations() {
        return this.workstations;
    }

    public Area workstations(Set<Workstation> workstations) {
        this.setWorkstations(workstations);
        return this;
    }

    /*public Area addWorkstation(Workstation workstation) {
        this.workstations.add(workstation);
        workstation.setArea(this);
        return this;
    }

    public Area removeWorkstation(Workstation workstation) {
        this.workstations.remove(workstation);
        workstation.setArea(null);
        return this;
    }*/

    public void setWorkstations(Set<Workstation> workstations) {
        /*if (this.workstations != null) {
            this.workstations.forEach(i -> i.setArea(null));
        }
        if (workstations != null) {
            workstations.forEach(i -> i.setArea(this));
        }*/
        this.workstations = workstations;
    }

    public Set<Storage> getStorages() {
        return this.storages;
    }

    public Set<Activity> getActivities() {
        return activities;
    }

    public Area storages(Set<Storage> storages) {
        this.setStorages(storages);
        return this;
    }

    public Area addActivity(Activity activity){
        activities.add(activity);
        return this;

    }

    /*public Area addStorage(Storage storage) {
        this.storages.add(storage);
        storage.setArea(this);
        return this;
    }

    public Area removeStorage(Storage storage) {
        this.storages.remove(storage);

        return this;
    }*/

    public void setStorages(Set<Storage> storages) {

        this.storages = storages;
    }

    public Facility getFacility() {
        return this.facility;
    }

    public Area facility(Facility facility) {
        this.setFacility(facility);
        return this;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Area)) {
            return false;
        }
        return id != null && id.equals(((Area) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Area{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
