package com.admin.app.domain;

import com.admin.app.domain.Enumeration.AreaType;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

public class AreaWithStorage implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID id;

    private UUID facilityId;

    private AreaType type;



    // jhipster-needle-entity-add-field - JHipster will add fields here
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public AreaWithStorage id(UUID id) {
        this.id = id;
        return this;
    }

    public UUID getFacilityId() {
        return this.facilityId;
    }

    public AreaWithStorage facilityId(UUID facilityId) {
        this.facilityId = facilityId;
        return this;
    }

    public void setFacilityId(UUID facilityId) {
        this.facilityId = facilityId;
    }

    public AreaType getType() {
        return this.type;
    }

    public AreaWithStorage type(AreaType type) {
        this.type = type;
        return this;
    }

    public void setType(AreaType type) {
        this.type = type;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AreaWithStorage)) {
            return false;
        }
        return id != null && id.equals(((AreaWithStorage) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Area{" +
            "id=" + getId() +
            ", facilityId='" + getFacilityId() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
