package com.admin.app.domain.Enumeration;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum AreaType {

        @JsonProperty("WORKSTATION_AREA") WORKSTATION_AREA,
        @JsonProperty("STORAGE_AREA") STORAGE_AREA,
        @JsonProperty("INWARD_AREA") INWARD_AREA,


}
