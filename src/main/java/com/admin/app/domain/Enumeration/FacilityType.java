package com.admin.app.domain.Enumeration;

public enum FacilityType {
    DELIVERY_CENTRE,
    COLLECTION_CENTRE,
    PROCESSING_UNIT,
    TRUCK,
    PLAZA,
    JETTY,
}
