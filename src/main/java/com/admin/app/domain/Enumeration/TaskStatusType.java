package com.admin.app.domain.Enumeration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;


public enum TaskStatusType {
    @JsonProperty("PENDING")PEDNING,
    @JsonProperty("STARTED") STARTED,
    @JsonProperty("ENDED") ENDED,
    @JsonProperty("CANCELLED") CACNCELLED,
}
