package com.admin.app.domain;

import com.admin.app.domain.Enumeration.TaskStatusType;
import com.fasterxml.jackson.databind.DeserializationFeature;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;

public class Task implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID id;

    private UUID facilityId;

    private UUID activityId;

    private String taskName;

    private String workflowInstanceId;

    private TaskStatusType status;

    private Instant etc;

    private Instant startTime;

    private Instant endTime;

    private String entityType;

    private UUID entityId;

    private UUID actorId;

    private String metadata;

    private Instant createdAt;

    private Instant updatedAt;

    private Instant deletedAt;

    private UUID createdBy;

    private UUID updatedBy;

    private UUID deletedBy;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public UUID getId() {
        return this.id;
    }

    public Task id(UUID id) {
        this.setId(id);
        return this;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getFacilityId() {
        return this.facilityId;
    }

    public Task facilityId(UUID facilityId) {
        this.setFacilityId(facilityId);
        return this;
    }

    public void setFacilityId(UUID facilityId) {
        this.facilityId = facilityId;
    }

    public UUID getActivityId() {
        return this.activityId;
    }

    public Task activityId(UUID activityId) {
        this.setActivityId(activityId);
        return this;
    }

    public void setActivityId(UUID activityId) {
        this.activityId = activityId;
    }

    public String getTaskName() {
        return this.taskName;
    }

    public Task taskName(String taskName) {
        this.setTaskName(taskName);
        return this;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getWorkflowInstanceId() {
        return this.workflowInstanceId;
    }

    public Task workflowInstanceId(String workflowInstanceId) {
        this.setWorkflowInstanceId(workflowInstanceId);
        return this;
    }

    public void setWorkflowInstanceId(String workflowInstanceId) {
        this.workflowInstanceId = workflowInstanceId;
    }

    public TaskStatusType getStatus() {
        return this.status;
    }

    public Task status(TaskStatusType status) {

        this.setStatus(status);
        return this;
    }

    public void setStatus(TaskStatusType status) {
        this.status = status;
    }

    public Instant getEtc() {
        return this.etc;
    }

    public Task etc(Instant etc) {
        this.setEtc(etc);
        return this;
    }

    public void setEtc(Instant etc) {
        this.etc = etc;
    }

    public Instant getStartTime() {
        return this.startTime;
    }

    public Task startTime(Instant startTime) {
        this.setStartTime(startTime);
        return this;
    }

    public void setStartTime(Instant startTime) {
        this.startTime = startTime;
    }

    public Instant getEndTime() {
        return this.endTime;
    }

    public Task endTime(Instant endTime) {
        this.setEndTime(endTime);
        return this;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }

    public String getEntityType() {
        return this.entityType;
    }

    public Task entityType(String entityType) {
        this.setEntityType(entityType);
        return this;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public UUID getEntityId() {
        return this.entityId;
    }

    public Task entityId(UUID entityId) {
        this.setEntityId(entityId);
        return this;
    }

    public void setEntityId(UUID entityId) {
        this.entityId = entityId;
    }

    public UUID getActorId() {
        return this.actorId;
    }

    public Task actorId(UUID actorId) {
        this.setActorId(actorId);
        return this;
    }

    public void setActorId(UUID actorId) {
        this.actorId = actorId;
    }

    public String getMetadata() {
        return this.metadata;
    }

    public Task metadata(String metadata) {
        this.setMetadata(metadata);
        return this;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public Instant getCreatedAt() {
        return this.createdAt;
    }

    public Task createdAt(Instant createdAt) {
        this.setCreatedAt(createdAt);
        return this;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Instant getUpdatedAt() {
        return this.updatedAt;
    }

    public Task updatedAt(Instant updatedAt) {
        this.setUpdatedAt(updatedAt);
        return this;
    }

    public void setUpdatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Instant getDeletedAt() {
        return this.deletedAt;
    }

    public Task deletedAt(Instant deletedAt) {
        this.setDeletedAt(deletedAt);
        return this;
    }

    public void setDeletedAt(Instant deletedAt) {
        this.deletedAt = deletedAt;
    }

    public UUID getCreatedBy() {
        return this.createdBy;
    }

    public Task createdBy(UUID createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(UUID createdBy) {
        this.createdBy = createdBy;
    }

    public UUID getUpdatedBy() {
        return this.updatedBy;
    }

    public Task updatedBy(UUID updatedBy) {
        this.setUpdatedBy(updatedBy);
        return this;
    }

    public void setUpdatedBy(UUID updatedBy) {
        this.updatedBy = updatedBy;
    }

    public UUID getDeletedBy() {
        return this.deletedBy;
    }

    public Task deletedBy(UUID deletedBy) {
        this.setDeletedBy(deletedBy);
        return this;
    }

    public void setDeletedBy(UUID deletedBy) {
        this.deletedBy = deletedBy;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Task)) {
            return false;
        }
        return id != null && id.equals(((Task) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Task{" +
            "id=" + getId() +
            ", facilityId='" + getFacilityId() + "'" +
            ", activityId='" + getActivityId() + "'" +
            ", taskName='" + getTaskName() + "'" +
            ", workflowInstanceId='" + getWorkflowInstanceId() + "'" +
            ", status='" + getStatus() + "'" +
            ", etc='" + getEtc() + "'" +
            ", startTime='" + getStartTime() + "'" +
            ", endTime='" + getEndTime() + "'" +
            ", entityType='" + getEntityType() + "'" +
            ", entityId='" + getEntityId() + "'" +
            ", actorId='" + getActorId() + "'" +
            ", metadata='" + getMetadata() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", deletedAt='" + getDeletedAt() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", deletedBy='" + getDeletedBy() + "'" +
            "}";
    }
}
