package com.admin.app.domain.request;




public class ManageUserRequest {

    private String facilityId;

    private String role;

    private User user;


    public String getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(String facilityId) {
        this.facilityId = facilityId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "ManageUserRequest{" +
            "facilityId='" + facilityId + '\'' +
            ", role='" + role + '\'' +
            ", user=" + user +
            '}';
    }
}
