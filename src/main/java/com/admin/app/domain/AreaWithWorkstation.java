package com.admin.app.domain;

import com.admin.app.domain.Enumeration.AreaType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AreaWithWorkstation implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID id;

    private UUID facilityId;

    private AreaType type;

    List<Workstation> workstationList;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public AreaWithWorkstation id(UUID id) {
        this.id = id;
        this.workstationList = new ArrayList<>();
        return this;
    }

    public UUID getFacilityId() {
        return this.facilityId;
    }

    public AreaWithWorkstation facilityId(UUID facilityId) {
        this.facilityId = facilityId;
        this.workstationList = new ArrayList<>();
        return this;
    }

    public void setFacilityId(UUID facilityId) {
        this.facilityId = facilityId;
    }

    public AreaType getType() {
        return this.type;
    }

    public AreaWithWorkstation type(AreaType type) {
        this.type = type;
        this.workstationList = new ArrayList<>();
        return this;
    }

    public void addWorkstation(Workstation workstation){
        workstationList.add(workstation);
    }

    public void setType(AreaType type) {
        this.type = type;
    }

    public List<Workstation> getWorkstationList() {
        return workstationList;
    }

// jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AreaWithWorkstation)) {
            return false;
        }
        return id != null && id.equals(((AreaWithWorkstation) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Area{" +
            "id=" + getId() +
            ", facilityId='" + getFacilityId() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
