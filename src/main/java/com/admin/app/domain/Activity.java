package com.admin.app.domain;


import java.util.UUID;

public class Activity {

    private static final long serialVersionUID = 1L;

    private UUID id;

    private UUID taskWorkflowId;

    private String name;

    private String description;

    private String entityAllocationType;
    private String action;



    // jhipster-needle-entity-add-field - JHipster will add fields here

    public UUID getId() {
        return this.id;
    }

    public Activity id(UUID id) {
        this.setId(id);
        return this;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getTaskWorkflowId() {
        return this.taskWorkflowId;
    }

    public Activity taskWorkflowId(UUID taskWorkflowId) {
        this.setTaskWorkflowId(taskWorkflowId);
        return this;
    }

    public void setTaskWorkflowId(UUID taskWorkflowId) {
        this.taskWorkflowId = taskWorkflowId;
    }

    public String getName() {
        return this.name;
    }

    public Activity name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public Activity description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEntityAllocationType() {
        return this.entityAllocationType;
    }

    public Activity entityAllocationType(String entityAllocationType) {
        this.setEntityAllocationType(entityAllocationType);
        return this;
    }

    public void setEntityAllocationType(String entityAllocationType) {
        this.entityAllocationType = entityAllocationType;
    }


    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Activity)) {
            return false;
        }
        return id != null && id.equals(((Activity) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Activity{" +
            "id=" + getId() +
            ", taskWorkflowId='" + getTaskWorkflowId() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", entityAllocationType='" + getEntityAllocationType() + "'" +
            "}";
    }
}
